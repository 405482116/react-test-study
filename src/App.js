import React from 'react';
import useFetch from './useFetch';
import { Outlet, Link, useNavigate } from "react-router-dom";

function App() {
  const [data, onClick] = useFetch('https://jsonplaceholder.typicode.com/todos/1');
  let navigate = useNavigate();

  return (
    <div>
      <h1>{data?.title} </h1>
      <nav
        style={{
          borderBottom: "solid 1px",
          paddingBottom: "1rem"
        }}
      >
        <Link to="/invoices">Invoices</Link> |{" "}
        <Link to="/expenses">Expenses</Link>
        <button onClick={() => onClick(navigate('/expenses'))}>Button </button>
      </nav>
      <Outlet />
    </div>
  );
}

export default App;
