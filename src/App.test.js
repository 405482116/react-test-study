import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { MemoryRouter, Routes, Route, useLocation } from 'react-router-dom';
import App from './App';

const mockClick = jest.fn(callback => {
  callback();
});
jest.mock("./useFetch", () => ({
  __esModule: true,
  default: () => ([{
    "userId": 1,
    "id": 1,
    "title": "delectus aut autem",
    "completed": false
  }, mockClick]),
}));
const mockedNavigator = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedNavigator
}));
describe('Name of the group', () => {
  test('renders learn react link', () => {
    render(<MemoryRouter>
      <App />
    </MemoryRouter>)
    expect(screen.getByRole('heading', { name: /delectus aut autem/i })).toBeInTheDocument();
  });
  test('should ', () => {
    const Wrapper = () => {
      return (
        <MemoryRouter initialEntries={["/my/initial/route"]}>
          <App />
        </MemoryRouter>)
    };
    render(<Wrapper />)


    expect(screen.getByRole('heading', { name: /delectus aut autem/i })).toBeInTheDocument();
    userEvent.click(screen.getByText(/Invoices/i));
    userEvent.click(screen.getByRole('button', { name: /Button/i }));
    expect(mockClick).toBeCalled();
    expect(mockedNavigator).toHaveBeenCalledWith('/expenses');
  });
});

